# Documentation


# Task1.2
Created Dockerfile

To build and run I used these commands (while inside task1.2 folder):

```
docker build -t task12:v2 .

docker run -d --name task12 -p 8080:8080 task12:v2

```

To check if it works i used curl and also implemented a healthcheck into the container:

```
curl http://localhost:8080
```

# Task 2.1

I setup the webdriver and Selenium test runner as seperate docker containers, and launched them as a single docker-compose stack:
```
docker-compose up -d
```
To check the tests results i used logs:
```
docker logs test_runner
```

To debug the test live VNC can be used:
```
http://localhost:7900/
password: secret
```
---

## VFZ technical assessment 2024050201

## Prerequisites

### Forking the repository

Create a public fork of this repository. Clone your fork of the repository on your local machine and work in this clone. Don't forget to commit and push your work!

### Hints

- For dockerizing java app, it's recommended to use [eclipse-temurin:21](https://hub.docker.com/_/eclipse-temurin/tags?page=&page_size=&ordering=&name=21) or newer.
- You may get extra credit for going beyond acceptance criteria, e.g. providing suggestions and improvements

## Assignments

### Task 1.1: Containerising a Spring Boot application

> If you complete optional task 1.2, you may skip this task 1.1

In the `task1.1` directory you will find a simple [Spring Boot](https://spring.io/projects/spring-boot) application that spins up a web server that prints `Hello World` when visited. The application is already compiled into a single binary executable. Your task is now to create a `Dockerfile` that runs the application.

**Acceptance criteria**

1. File `./task1.1/Dockerfile` created
2. Image can be built
3. When container runs it keeps TCP port XXXX open
4. `curl http://localhost:xxxx` or opening browser should render "Hello World!"
5. Clear instructions provided how to build and run it

### Task 1.2: Building a Sprint Boot application and containerising it

> This is optional task for extra credit. It can be ignored<br>
> If you complete this task, you may skip task1.1

Compile the Spring Boot application inside Docker before running it. 

If you are not familiar with Gradle, you can use the Gradle Wrapper with command `./gradlew clean build` to generate an executable `.jar` file that can be used within the container. After building, this file will be located in the `task1.2` directory under `build/libs/helloworld.jar`.

**Acceptance criteria**

1. File `./task1.2/Dockerfile` created
2. When image is built, the compilation occurs, and java sources are compiled into a single `.jar` binary
3. When container runs it keeps TCP port XXXX open
4. `curl http://localhost:xxxx` or opening browser should render "Hello World!"
5. Clear instructions provided how to build and run it

## Task 2.1

Using Selenium, successfully process 3 (three) test cases against https://practicetestautomation.com/practice-test-login/ page: Positive LogIn test, Negative username test, Negative password test. The task can be completed in various ways, feel free to choose approach that you prefer.

- You may dockerize it or run as standalong application on host (your choice).
- You may use Python, JavaScript or Java
- You may use any web driver or multiple
- You may use single instance or Selenium Grid

**Acceptance criteria**

1. `./task2.1` contains all necessary files to run and/or build the test
2. Test runs and provides comprehensive output whether it's success or failure
3. Clear instructions provided how to build and run it
