from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os



remote_webdriver_url = os.getenv('REMOTE_WEBDRIVER_URL')
website_url = 'https://practicetestautomation.com/practice-test-login/'
username = 'student'
password = 'Password123'
bad_username = 'studentas'
bad_password = 'password123'


def get_driver():
    chrome_options = Options()
    return webdriver.Remote(command_executor=remote_webdriver_url, options=chrome_options)

# Error handling
def handle_errors (func):
    def wrapper(*args, **kwargs):
        try: 
            func(*args, **kwargs)
        except AssertionError as e:
            print(f"AssertionError occured: {e}")
    return wrapper

# Function to perform the login test
@handle_errors
def perform_login_test(driver):
    driver.get(website_url)
    username_field = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'username')))
    password_field = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'password')))

    username_field.send_keys(username)
    password_field.send_keys(password)
        
    driver.find_element(By.ID, 'submit').click()

    assert "login" not in driver.current_url.lower(), "Login failed!"
    print("Login successful!")


# Function to perform username test
@handle_errors
def perform_username_test(driver):
        driver.get(website_url)
        username_field = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'username')))
        password_field = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'password')))
        
        username_field.send_keys(bad_username)
        password_field.send_keys(password)
        
        driver.find_element(By.ID, 'submit').click()
        
        expected_msg= "Your username is invalid!"
        err_msg_box = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="error"]')))
        displayed_error = err_msg_box.text
        assert displayed_error == expected_msg, "Displayed error is not as Expected"
        print("Test pass (username is invalid)")
        


# Function to perform password test
@handle_errors
def perform_password_test(driver):
        driver.get(website_url)
        username_field = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'username')))
        password_field = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'password')))
        
        username_field.send_keys(username)
        password_field.send_keys(bad_password)
        
        driver.find_element(By.ID, 'submit').click()
        
        expected_msg= "Your password is invalid!"
        err_msg_box = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="error"]')))
        displayed_error = err_msg_box.text
        assert displayed_error == expected_msg, "Displayed error is not as Expected"
        print("Test pass (password is invalid)")
        


if __name__ == '__main__':
    try:
        driver = get_driver()
        print("Performing positive login test:")
        perform_login_test(driver)
        print("Performing negative username test:")
        perform_username_test(driver)
        print("Performing negative password test:")
        perform_password_test(driver)
    finally:
        driver.quit()
